/**
 * This program runs as a server and controls the force to be applied to balance the Inverted Pendulum system running on the clients.
 */
import java.io.*;
import java.net.*;
import java.util.*;

public class PoleServer
{
	//test
	private static ServerSocket serverSocket;
	private static final int port =2012;
	
	/**
     * Main method that creates new socket and PoleServer instance and runs it.
     */
	public static void main(String [] args) throws IOException
	{
		try
		{ serverSocket = new ServerSocket(port); }		

		catch (IOException ioe)
		{
		System.out.println("unable to set up port");
		System.exit(1);
		}
		System.out.println("Waiting for connection");						
		do
		{
			Socket client = serverSocket.accept();
			System.out.println("\nnew client accepted.\n");
			PoleServer_handler handler = new PoleServer_handler(client);
		}while(true);
	}
}


/**
 * This class sends control messages to balance the pendulum on client side.
 */ 
class PoleServer_handler implements Runnable {
	static ServerSocket providerSocket;
	Socket connection = null;
	ObjectOutputStream out;
	ObjectInputStream in;
	String message="abc";
	static Socket client;
	Thread t;
	
	/**
	 * Class Constructor
	 */
	public PoleServer_handler(Socket socket)
	{
		t=new Thread(this);
		client = socket;
		
		try 
		{
		out = new ObjectOutputStream(client.getOutputStream());
		out.flush();
		in = new ObjectInputStream(client.getInputStream());
		}
		catch(IOException ioe)
		{ ioe.printStackTrace(); }
		t.start();
	}
	double angle, action=0, prevAngle, angleDiff=0, i=0;
	
	
	/**
	 * This method receives the pole positions and calculates the updated values and sends them across to the client.
	 * It also sends the amount of force to be applied to balance the pendulum.
	 * @throws ioException
	 */
	void control_pendulum(ObjectOutputStream out, ObjectInputStream in) {
	try{
		do{
			{
					prevAngle = in.readDouble();
					angle = in.readDouble();
					System.out.println("here"+prevAngle);
					System.out.println("here"+angle);
					// Merging TDMA
					//long client_time = in.readLong();
					
					angleDiff=prevAngle-angle;
					System.out.println("diff:" + angleDiff);
					
					if (angle > 0 && angleDiff<0)
					{
					System.out.println("inside angle>0");
					System.out.println("angle = " + angle);
					System.out.println("action = " + action);
					if (angle > 65*0.01745) {action= 10;}
					else if (angle > 60*0.01745) {action= 8;}
					else if (angle > 50*0.01745) {action= 7.5;}
					else if (angle > 30*0.01745) {action= 0.02;}
					else if (angle > 20*0.01745) {action= 0.01;}
					else if (angle > 10*0.01745) {action= 0.0095;}
					else if (angle < 0.0055) {/*resetPole=1;*/}
					else
						{action = 0.1;}
					}
	  
					else if (angle < 0 && angleDiff>0)
					{
					System.out.println("inside angle<0");
					System.out.println("angle = " + angle);
					System.out.println("action = " + action);
	  
					if (angle < -65*0.01745) {action= -10;}
					else if (angle < -60*0.01745) {action= -8;}
					else if (angle < -50*0.01745) {action= -7.5;}
					else if (angle < -30*0.01745) {action= -0.02;}
					else if (angle < -20*0.01745) {action= -0.01;}
					else if (angle < -10*0.01745) {action= -0.0095;}
					else if (angle > -0.0055) {/*resetPole=1;*/}
					else
						{action = -0.1; }
					}
					else 
					{ 
					System.out.println("inside angle=0");
					System.out.println("angle = " + angle);
					System.out.println("action = " + action);
					action = 0.;
					}
					
					
					sendMessage_double(action);
					
					
					if (message.equals("bye"))
						sendMessage("bye");
				}
		
				
			}while(!message.equals("bye"));
		}
		
		catch(IOException ioException){
			ioException.printStackTrace();
		}
		
		try
		{
		 if(client!=null)
		 { System.out.println("closing down connection ...");
			client.close();
		 }
		}

		catch(IOException ioe)
		{
		System.out.println("unable to disconnect");
		}
	}
	
	/**
	 * This method calls the controller method to balance the pendulum.
	 * @throws ioException
	 */
	public void run() 	{
		
		try{
			control_pendulum(out, in);
			
		}
		catch(Exception ioException){
			ioException.printStackTrace();
		}
		finally{
			
		}
	
	}
	
	/**
	 * This method sends the String message on the object output stream.
	 * @throws ioException
	 */
	void sendMessage(String msg)
	{
		try{
			out.writeObject(msg);
			out.flush();
			System.out.println("server>" + msg);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
	
	/**
     * This method sends the Double message on the object output stream.
	 * @throws ioException
     */ 
	void sendMessage_double(double msg)
	{
		try{
			out.writeDouble(msg);
			out.flush();
			System.out.println("server>" + msg);
		}
		catch(IOException ioException){
			ioException.printStackTrace();
		}
	}
	
	
}
  