/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package timer_client;
import java.io.*;
import java.net.*;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Date;



class ShareData{
    

    private long offset;   // clock difference between the slave and the server
    private final long latency = 200; // network latency, you need to measure it by yourself
    
    private final long timeslotlen = 1000; // 1 second
    private final long broadcastperiod = 10000; // 10 seonds
    
    private long startTime; // timestamp of start of 10 second period
    
    
    // offset will be shared by at least two threads. Hence, it should be protected
     void update_offset(long offset)
    {
        synchronized(this)
        {
        this.offset = offset;
        }
    }
    
     long  get_offset()
    {
        synchronized(this)
        {
        return this.offset;
        }
    }
     
     void update_startTime(long timestamp){
    	 synchronized(this)
    	 {
    		 this.startTime = timestamp;
    	 }
     }
     
     long get_startTime(){
    	 synchronized(this)
    	 {
    		 return this.startTime;
    	 }
     }
    
}

// Note: Implementation of this class is not complete!
class SlaveSending extends TimerTask {
      ShareData data;  
      ObjectOutputStream out;
      Timer timer;
      
      // data and out should be initialized
      SlaveSending(ShareData d, ObjectOutputStream os)
      {
          this.data = d;
          this.out = os;
      }
      // slave should send out current local time every X seconds (here X = 2)
      // The code only contains the sending part. You need to finish the left
      // to make sure that the clicent is synchronized with the server
      // 
      public void run()
      {
          try
          {
              //out.writeLong(System.currentTimeMillis());
              //out.flush();
        	  while(System.currentTimeMillis() < data.get_startTime());
              
              // More code needed here
              while(System.currentTimeMillis() < data.get_startTime() + 10000){
            	  //if(System.currentTimeMillis() % 2000 == 0){
            		  System.out.println("Sending: " + System.currentTimeMillis());
            		  out.writeLong(System.currentTimeMillis());
            	  	  out.flush();
            	  	  long time = System.currentTimeMillis();
            	  	  while(System.currentTimeMillis() < time + 2000);
            	  //}
              }
          }catch(Exception e)
          {
              e.printStackTrace();
          }
      }
}

/**
 *
 * @author xmzhu
 */
public class Timer_client {

    Socket requestSocket;
	ObjectOutputStream out;
 	ObjectInputStream in;
 	
        
        ShareData data;
        Timer clienttimer;
      
        
        Timer_client(){}
    
             
        void ConnectServer()
        {
		    //try {requestSocket = new Socket("10.0.1.100", 8000);
        	try {requestSocket = new Socket("localhost", 1000);
			System.out.println("Connected to localhost in port 1000");
			//2. get Input and Output streams
			out = new ObjectOutputStream(requestSocket.getOutputStream());
			out.flush();
			in = new ObjectInputStream(requestSocket.getInputStream()); }
		
	catch(IOException ioException){
	ioException.printStackTrace();
	}		
        }
                
   
   
    
        // wait for broadcast and syn itself
        void client_sync()
        {
        	SlaveSending s = new SlaveSending(this.data, this.out);
        	long offset = 0;
        	long previousTime = 0;
             try
             {
                 while (true)
                 {
                     long actual_server_time = in.readLong(); 
                     long localTime = System.currentTimeMillis();
                     System.out.println("Timestamp: " + System.currentTimeMillis());
                     System.out.println("Message: " + actual_server_time);
                     System.out.println("Offset: " + offset);
                     System.out.println("PreviousTime: " + previousTime);
                     //out.writeLong(System.currentTimeMillis() + 1000);
                     //out.flush();
                     // Write your code here !
                     data.update_offset(System.currentTimeMillis() - actual_server_time);
                     if(previousTime != 0){
                    	 offset = (localTime - previousTime) - 10000;
                    	 if(offset > 0)
                        	 data.update_startTime(localTime + offset);
                         else
                        	 data.update_startTime(localTime - offset);
                    	 
                    	 //offset = (localTime - previousTime) - 10000;
                     }
                     else{
                    	 data.update_startTime(localTime);
                     }
                     s = new SlaveSending(this.data, this.out);
                     s.run();
                                      
                     previousTime = data.get_startTime();
                     
                     
                     
                     //System.out.println("Scheduled: " + (System.currentTimeMillis() + 1000 + 2000 + data.get_offset()));
                     //data.update_offset(System.currentTimeMillis() - 100);
                     //clienttimer.schedule(new SlaveSending(this.data, this.out), new Date(System.currentTimeMillis() + 2000));
                     
                     //clienttimer.cancel();
                     //clienttimer.schedule(new SlaveSending(this.data, this.out), 1000,2000);
                     
                 }
             }catch(Exception e)
             {
                 e.printStackTrace();
             }
        }
   
    // Let the client sends out packet to the server every X seconds (X == 2)
    public static void main(String[] args) {
        // TODO code application logic here
        Timer_client client = new Timer_client();
        ShareData data = new ShareData();
        client.clienttimer = new Timer();

        client.data = data;
        client.ConnectServer();
        
        // Schedule sending packets to server every 2 seconds
        //client.clienttimer.schedule(new SlaveSending(data, client.out),2000,2000);
        client.client_sync();

    }
}
